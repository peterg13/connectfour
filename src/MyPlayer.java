import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard, 6, this.playerNumber);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        //if open first move will be the center column
        int[][] intBoard = gameBoard.getBoard();
        if(intBoard[0][3] == 0){
            Move move = new Move(3);
            return move;
        }

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber) {
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0
        double moveValue = 0.0;
        moveValue = threeInARow(gameBoard, playerNumber);
        if(moveValue == 0.0){
            moveValue = twoInARow(gameBoard, playerNumber);
        }
        return moveValue;


    }


    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }

    private double threeInARow(Board gameBoard, int playerNumber){
        int[][] intBoard = gameBoard.getBoard();
        double counter = 0.0;
        String str = "";
        if(playerNumber == 1) {
            //scans each row for 3 in a row
            for (int x = 0; x < 7; x++) {
                for (int y = 0; y < 7; y++) {
                    str = str + intBoard[x][y];
                }

                if (str.contains("111")) {
                    counter = 1.0;
                    return counter;
                }

                str = "";

            }
            //scans each column for 3 in a row
            for (int y = 0; y < 7; y++) {
                for (int x = 0; x < 7; x++) {
                    str = str + intBoard[x][y];
                }

                if (str.contains("111")) {
                    counter = 1.0;
                    return counter;
                }

                str = "";

            }
        }
        else{
            //scans each row for 3 in a row
            for (int x = 0; x < 7; x++) {
                for (int y = 0; y < 7; y++) {
                    str = str + intBoard[x][y];
                }

                if (str.contains("222")) {
                    counter = -1.0;
                    return counter;
                }

                str = "";

            }
            //scans each column for 3 in a row
            for (int y = 0; y < 7; y++) {
                for (int x = 0; x < 7; x++) {
                    str = str + intBoard[x][y];
                }

                if (str.contains("222")) {
                    counter = -1.0;
                    return counter;
                }

                str = "";

            }
        }
        return counter;
    }

    private double twoInARow(Board gameBoard, int playerNumber){
        int[][] intBoard = gameBoard.getBoard();
        double counter = 0.0;
        String str = "";
        if(playerNumber == 1) {
            //scans each row for 2 in a row
            for (int x = 0; x < 7; x++) {
                for (int y = 0; y < 7; y++) {
                    str = str + intBoard[x][y];
                }

                if (str.contains("11")) {
                    counter = 0.5;
                    return counter;
                }

                str = "";

            }
            //scans each column for 2 in a row
            for (int y = 0; y < 7; y++) {
                for (int x = 0; x < 7; x++) {
                    str = str + intBoard[x][y];
                }

                if (str.contains("11")) {
                    counter = 0.5;
                    return counter;
                }

                str = "";

            }
        }
        else{
            //scans each row for 2 in a row
            for(int x = 0; x < 7; x++){
                for(int y = 0; y<7; y++){
                    str = str + intBoard[x][y];
                }

                if(str.contains("22")){
                    counter = -0.5;
                    return counter;
                }

                str = "";

            }
            //scans each column for 2 in a row
            for(int y = 0; y < 7; y++){
                for(int x = 0; x<7; x++){
                    str = str + intBoard[x][y];
                }

                if(str.contains("22")){
                    counter = -0.5;
                    return counter;
                }

                str = "";

            }
        }
        return counter;
    }




}
